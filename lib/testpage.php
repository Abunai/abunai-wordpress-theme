<?php

/*
Template Name: TestPage
*/

add_action( 'genesis_loop', 'page_outdated_loop', 9 );

function page_outdated_loop() {
	if( get_field('page_outdated') == true ) {
		the_field('page_outdated_message');
	}
}

//Run the genesis loop
genesis();

?>