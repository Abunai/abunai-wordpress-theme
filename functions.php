<?php
//* Start the engine
include_once( get_template_directory() . '/lib/init.php' );

//* Child theme (do not remove)
define( 'CHILD_THEME_NAME', 'Abunai Theme' );
define( 'CHILD_THEME_URL', 'http://www.studiopress.com/' );
define( 'CHILD_THEME_VERSION', '0.12.2' );

//* Enqueue Google Fonts
add_action( 'wp_enqueue_scripts', 'genesis_sample_google_fonts' );
function genesis_sample_google_fonts() {
	wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Lato:300,400,700', array(), CHILD_THEME_VERSION );
}

//* Add HTML5 markup structure
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );

//* Add viewport meta tag for mobile browsers
add_theme_support( 'genesis-responsive-viewport' );

//* Add support for 3-column footer widgets
add_theme_support( 'genesis-footer-widgets', 3 );

//* TODO This add's support but it's different from the existing one
add_theme_support( 'genesis-custom-header', array( 'width' => 1200, 'height' => 250 ) );
add_theme_support( 'custom-background', array(
  'default-image' => get_stylesheet_directory_uri() . 'images/background.png',
  'default-color' => 'FFF'
));

//* Remove secondary Sidebar
unregister_sidebar( 'sidebar-alt' );
unregister_sidebar( 'header-right');


//* Load all elements of child theme
include_once( 'lib/settings.php' );
include_once( 'lib/header.php' );
include_once( 'lib/body.php' );
include_once( 'lib/handleiding.php' );
include_once( 'lib/menu.php' );
include_once( 'lib/vacancy.php' );

//* Customize the post info function
add_filter( 'genesis_post_info', 'sp_post_info_filter' );
function sp_post_info_filter($post_info) {
if ( !is_page() ) {
  $post_info = '[post_date] [post_comments] [post_edit]';
  return $post_info;
}}

//* Page outdated message on selected pages
add_action( 'genesis_loop', 'page_outdated_loop', 9 );
function page_outdated_loop() {
	if( get_field('page_outdated') == true ) {
		the_field('page_outdated_message');
	}
}
