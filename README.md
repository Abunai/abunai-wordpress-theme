# Abunai Genesis Theme

This theme is made for the Abunai! Convention in the Netherlands.
It''s using the Genesis Framework to keep the base simple.

## Shortcodes

### [vacancy]
Generates a list of entries of the Vacancy post type

## Authors

* Bas Grolleman <bgrolleman@emendo-it.nl>


